/**
 * Created by cristian on 26.02.2017.
 */

$(document).ready(function(){
    setTimeout(function(){
        if(typeof profile === 'undefined'){
            console.log('You\'re not logged in!');
            $('#modal-trigger').click();
        }else{
            $('.page-header .row .col-md-8').append(
                '<h1 id="profile_name">Welcome ' + profile.getGivenName() + '</h1>' +
                '<a class="sign-out" href="" onclick="signOut();">Sign out</a>'
            )
            $('.page-header .row .col-md-4').append(
                '<p id="profile_email"> ' + profile.getEmail() + '</p>'+
                '<img id="profile_image" src="' + profile.getImageUrl() + '">'
            )
            $('.jumbotron').css('display', 'none');
            $('.photo-container, #map, #pano, .page-header').css('display', 'block');
            initialize();
        }
    }, 1500);
})

var createCookie = function(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

var profile;

function onSignIn(googleUser) {
    profile = googleUser.getBasicProfile();
    if(getCookie('reload') != 'no'){
        createCookie('reload', 'no');
        window.location.reload();
    }
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        createCookie('reload', 'yes');
        window.location.reload();
    });
}

function initialize() {
    var pos = {
        lat: 47.151726,
        lng: 27.587914
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        center: pos,
        zoom: 14
    });

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);

            var astorPlace = new google.maps.LatLng(pos.lat, pos.lng);
            var webService = new google.maps.StreetViewService();
            /**Check in a perimeter of x meters**/
            var checkaround = 500;
            /** checkNearestStreetView is a valid callback function **/
            webService.getPanoramaByLocation(astorPlace, checkaround, checkNearestStreetView);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    function checkNearestStreetView(panoData) {
        if (panoData) {
            if (panoData.location) {
                if (panoData.location.latLng) {
                    var pano_pos = {
                        lat: panoData.location.latLng.lat(),
                        lng: panoData.location.latLng.lng(),
                    };
                    var panorama = new google.maps.StreetViewPanorama(
                        document.getElementById('pano'), {
                            position: pano_pos,
                            pov: {
                                heading: 34,
                                pitch: 10
                            }
                        });
                    var infoWindow = new google.maps.InfoWindow({map: map});
                    var my_location = ''
                    panorama.addListener('position_changed', function(){
                        var my_pos = {
                            lat: panorama.getPosition().lat(),
                            lng: panorama.getPosition().lng(),
                        };


                        var geocoder = new google.maps.Geocoder;

                        geocoder.geocode({'location': my_pos}, function(results, status) {
                            if(status == 'OK'){
                                my_location = results[1].formatted_address;
                                infoWindow.setContent(my_location);
                                infoWindow.setPosition(my_pos);
                                var options = {
                                    "api_key": "01ef001b80dac301d835cf415fef9925",
                                    "method": "flickr.photos.search", // You can replace this with whatever method,
                                    // flickr.photos.search fits your use case best, though.
                                    "format": "json",
                                    "nojsoncallback": "1",
                                    "text": results[2].formatted_address,  // This is where you'll put your "file name"
                                    'accuracy': '11',
                                    'lat': my_pos.lat,
                                    'lng': my_pos.lng,
                                }


                                makeFlickrRequest(options, function(data) {
                                    console.log(results[2].formatted_address)
                                    $('.my-photo').remove();
                                    var parsedData = JSON.parse(data);
                                    if(parsedData.stat == 'ok'){
                                        myLength = parsedData.photos.total;
                                        var myPhotos = parsedData.photos.photo
                                        for(var i = 0; i < myLength; i ++){
                                            var photoUrl = 'https://farm'+ myPhotos[i].farm +'.staticflickr.com/' +
                                                myPhotos[i].server + '/'+ myPhotos[i].id +'_'+myPhotos[i].secret+'.jpg'
                                            console.log(photoUrl)
                                            $('.photo-container').append(
                                                '<div class="my-photo" style="background-image: url('+photoUrl+')"></div>'
                                            )
                                        }

                                    }
                                });

                            }
                        });



                        // makeFlickrRequest(options, function(data) { alert(data) }); // Leaving the actual



                    });
                    map.setStreetView(panorama);
                }
            }
        }
    }

}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
}

var makeFlickrRequest = function(options, cb) {
    var url, xhr, item, first;

    url = "https://api.flickr.com/services/rest/";
    first = true;

    for (item in options) {
        if (options.hasOwnProperty(item)) {
            url += (first ? "?" : "&") + item + "=" + options[item];
            first = false;
        }
    }

    xhr = new XMLHttpRequest();
    xhr.onload = function() { cb(this.response); };
    xhr.open('get', url, true);
    xhr.send();
};