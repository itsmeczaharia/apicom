from django.apps import AppConfig


class ApicomConfig(AppConfig):
    name = 'apicom'
