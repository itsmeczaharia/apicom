from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def index(request):
    return render(request, 'apicom/index.html')


@csrf_exempt
def login(request):
    return render(request, 'apicom/login.html')
