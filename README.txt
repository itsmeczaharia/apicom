Dependencies:
	python 3.5
	django 1.10

To install:
	pip install django==1.10
	
To run:
	cd /path/to/apicom
	linux: python3 manage.py runserver
	windows: py -3 manage.py runserver